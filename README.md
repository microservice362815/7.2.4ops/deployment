# 「7.2.4 GitOpsの実装」のデプロイマニフェスト

7章 Argo CDの実装で利用するデプロイマニフェストです。
なお、「7.1.2 CIパイプラインの実装」で説明したプライベートなコンテナレジストリのコンテナイメージを利用するための内容は省略していますので、適切に設定してください。

## 利用するツール

### Kustomize

環境差分をKustomizeを利用して管理します。Kustomizeの詳細は、公式ページ [Kustomize](https://kustomize.io/) をご参照ください。

Kustomizeを利用する際のディレクトリ構成例を示します。

```bash
.
├── base
│ ├── deployment.yaml
│ └── kustomization.yaml
└── overlays
  └── prod
    └── kustomization.yaml
```

`base` ディレクトリでは各環境のベースとなるマニフェストを定義します。 `overlays` ディレクトリ配下のファイルで、環境差分のパッチ情報を管理します。

`kustomization.yaml` は、Kustomizeの管理設定を定義するマニフェストです。 `base` ディレクトリ、および `overlays` 直下の各環境の環境差分を管理するディレクトリ配下にも `kustomization.yaml` を格納する必要があります。

#### Kustomizeのインストール

（Homebrewの利用を前提とし）次のコマンドを実行し、Kustomizeをインストールできます。

```bash
$ brew install kustomize
// omit
```

#### Kustomizeを利用した各環境のデプロイマニフェスト生成

次のようなコマンドを実行することで、パッチ情報を適用したマニフェスト情報を生成できます。

```bash
$ kustomize build -f overlays/prod
// omit
```

また、次のようなコマンドを実行すると、差分のみを確認できます。

```bash
$ diff -u <(kustomize build base) <(kustomize build overlays/prod)
// omit
```

#### Kustomizeを利用した各環境のデプロイマニフェスト適用

次のようなコマンドを実行することで、パッチ情報を適用したマニフェスト情報をKubernetesクラスタへ適用できます。

```bash
$ kustomize build -f overlays/prod | kubectl apply -f -
// omit
```

## 各ファイルの説明

ディレクトリ構成は、環境差分をKustomizeを利用して管理するために以下のような構成をとっています。

```bash
.
├── base
│ ├── deployment.yaml
│ └── kustomization.yaml
└── overlays
  └── prod
    └── kustomization.yaml
```

### `base/deployment.yaml`

各環境のベースとなるデプロイマニフェストを定義します。 `base` ディレクトリ内のデプロイマニフェストのみでも動作するように定義することが望ましいです。

#### `base/deployment.yaml` の設定

|プロパティ|説明|設定例|
|`spec.template.spec.containers[0].image`|デプロイするコンテナイメージ名|`gihyo-ms-dev-book/catalogue:latest`|

### `overlays/prod/kustomization.yaml`

`overlays` ディレクトリの直下には、環境単位でディレクトリを作成します。今回は、 `prod` のみ作成しています。 `kustomization.yaml` に環境差分のパッチ情報を定義します。パッチ情報は別のマニフェストに定義することも可能です。

|プロパティ|説明|設定例|
|--------|----|----|
|`patches[0].patch`|デプロイするコンテナイメージ名|`gihyo-ms-dev-book/catalogue:v0.1`|
